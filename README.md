# Nuxt 3 Archi Landing (slider landing)

## Setup

- [NuxtImage](https://image.nuxt.com/) is used for displaying images as it has fancy options and looks cool 😎.
- [Vueuse](https://vueuse.org/). I used few functions from it (onClickOutside, useIntervalFn, useMediaQuery) in order not to write it from scratch.

I didn't add linter bc I was lazy I suppose, but I lint it myself and still looks good.

## App

Every animation is done by hands using only @keyframes or Vue Transition.

Every component was split to vue and scss file as they are too big to be in one file, so it much more readable and can be supported easily.

## Time spent: 13h straight w/o breaks 🥴
Spend 1-2h just to deploy this, oof (never devops)
