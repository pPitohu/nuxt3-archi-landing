// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ['~/assets/scss/main.scss'],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            @import "@/assets/scss/_breakpoints.scss";
            @import "@/assets/scss/_mixins.scss";
            @import "@/assets/scss/_fonts.scss";
            @import "@/assets/scss/_variables.scss";
          `
        }
      }
    },
  },
  modules: ["@nuxt/image", '@vueuse/nuxt'],
  runtimeConfig: {
    public: {
      baseURL: process.env.BASE_URL,
    },
  },
  nitro: {
    prerender: {
      routes: [
        '/_ipx/q_80/images/interior.jpg',
        '/_ipx/q_80/images/architecture.jpg',
        '/_ipx/_/icons/close.svg',
      ]
    }
  }
})