export const Locales = {
  ru: 'RUS',
  en: 'ENG'
}

export const useLocale = () => {
  return useState<string>('locale', () => useDefaultLocale().value)
}

export const useDefaultLocale = (fallback = 'en') => {
  const locale = ref(fallback)
  if (import.meta.server) {
    const reqLocale = useRequestHeaders()['accept-language']?.split(',')[0].slice(0,2)
    if (reqLocale) {
      locale.value = reqLocale
    }
  } else if (import.meta.client) {
    const navLang = navigator.language.slice(0,2)
    if (navLang) {
      locale.value = navLang
    }
  }
  if (!Object.keys(Locales).includes(locale.value))
    locale.value = 'en'

  return locale
}
