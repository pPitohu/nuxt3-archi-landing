type useFetchType = typeof useFetch

export const useCustomFetch: useFetchType = (path, options = {}) => {
  const config = useRuntimeConfig()
  options.baseURL = config.public.baseURL
  return useFetch(path, options)
}